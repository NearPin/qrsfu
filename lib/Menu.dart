import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:qr_sfu/Scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qr_sfu/Schedule.dart';

import 'BLoC.dart';

class Menu extends StatefulWidget{
  BLoC bloc;
  Menu(this.bloc);
  @override
  State<StatefulWidget> createState() => MenuState(bloc);

}
class MenuState extends State{
  BLoC bloc;
  MenuState(this.bloc);
  @override
  Widget build(BuildContext context) {
    return Schedule(bloc);
  }

}