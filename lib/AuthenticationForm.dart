import 'package:flutter/material.dart';
import 'package:qr_sfu/Menu.dart';
import 'package:flutter/services.dart';
import 'package:qr_sfu/RegistrastionForm.dart';
import 'BLoC.dart';


class Authentication extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyFormState();
}

class MyFormState extends State {
  final _formKey = GlobalKey<FormState>();
  String _username;
  String _password;
  BLoC bloc = new BLoC();

  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      appBar: AppBar(title: Text('SFU QR'),backgroundColor: Colors.deepOrange,),
      body: Builder( builder: (context) => Container(padding: EdgeInsets.all(10.0), child: new Form(key: _formKey, child: new Column(children: <Widget>[
      new TextFormField(decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'E-mail'
      ),
          validator: (value){
        if (value.isEmpty) return 'Пожалуйста введите свой Email';

        String p = "[a-zA-Z0-9+.\_\%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})+";
        RegExp regExp = new RegExp(p);

        if (regExp.hasMatch(value)) {
          _username = value;
          return null;
        }

        return 'Это не E-mail';
      }),
      new SizedBox(height: 20,),
      new TextFormField(obscureText: true, decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Пароль'
      ),
          validator: (value){
        if (value.isEmpty) return 'Пожалуйста введите ваш пароль';
        else
          _password = value;
          return null;
      }),

      new SizedBox(height: 20.0),

      new RaisedButton(onPressed: () async {
        if(_formKey.currentState.validate()) {
          Color color = Colors.red;
          String text;
          int statusCode = await bloc.authenticateUser(_username, _password);
          if(statusCode == BLoC.SUCCESFULL_RESOPONCE_CODE){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Menu(bloc)));
          }
          if(statusCode == BLoC.AUTHENTICATE_TRYING_TO_ENTER_FROM_ALIEN_DEVICE_CODE){
            text = 'Попытка входа с чужого устройства';
            color = Colors.red;
            Scaffold.of(context).showSnackBar(SnackBar(content: Text(text), backgroundColor: color,));
          }
          else
            {
              text = 'Проверьте правильность введенных данных';
              color = Colors.red;
              Scaffold.of(context).showSnackBar(SnackBar(content: Text(text), backgroundColor: color,));
            }
        }
      }, child: Text('Отправить'), color: Colors.deepOrange, textColor: Colors.white,),
        new RaisedButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Registration()));
        },child: Text("Регистрация",), color: Colors.deepOrange, textColor: Colors.white,),
    ],)))));
  }
  Future<bool> checkUserData(String userName,String password) async{
    return  true;
  }
}