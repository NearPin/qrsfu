import 'package:flutter/material.dart';
import 'package:qr_sfu/Schedule.dart';
import 'AuthenticationForm.dart';

void main() => runApp(
    new MaterialApp(
        debugShowCheckedModeBanner: false,
        /*initialRoute: '/authentication',
        routes: {
          '/authentication':(BuildContext context)=>Authentication(),
        },*/
        home: Authentication(),
    )
);
