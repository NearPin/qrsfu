import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_sfu/Scanner.dart';

import 'BLoC.dart';

class Schedule extends StatefulWidget{
  BLoC bLoC;
  Schedule(this.bLoC);
  @override
  State<StatefulWidget> createState() => ScheduleState(bLoC);
}
class ScheduleState extends State{
  BLoC bloc;
  List<ScheduleInfo> scheduleList;
  ScheduleState(this.bloc){
    setScheduleListState();
  }
  void setScheduleListState () async{
    scheduleList = await bloc.getScheduleInfo();
    setState(() {
    });
  }

  @override

  Widget build(BuildContext context) {
    if(scheduleList!=null){
      return Scaffold(
        appBar: AppBar(title: Text('Расписание'), backgroundColor: Colors.deepOrange,),
        body: Container(child:
        Center(
          child: Column(children: <Widget>[
            Container(
              width: 500,
              height: 500,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0),color: Colors.white),
              child: (
                  ListView.builder(itemBuilder: (BuildContext context,int index){
                    ScheduleInfo scheduleSub = scheduleList[index];
                    return GestureDetector(
                      child: ListScheduleItem(scheduleSub),
                      onTap: () async {
                        await Navigator.push(context, MaterialPageRoute(builder: (context)=> Scanner(scheduleSub,bloc)));
                        setState(() {
                        });
                        },
                    );
                  },itemCount: scheduleList.length,)
              ),
            ),
          ],
          ),
        ),
          color: Colors.deepOrange,
        ),
      );
    }else{
      return Scaffold(
        appBar: AppBar(title: Text('Расписание'), backgroundColor: Colors.deepOrange,),
        backgroundColor: Colors.deepOrange,
        body: Center(child:  CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.white),),
        ),
      );
    }

  }
  
}
class ListScheduleItem extends StatelessWidget{
  ScheduleInfo scheduleSub;
  ListScheduleItem(this.scheduleSub);

  @override
  Widget build(BuildContext context) {
    if(scheduleSub.isAttendanced){
      return FlatButton(child: Container(child: Column(children: <Widget>[
        Center(child: Text(scheduleSub.subject.name,style: TextStyle(fontSize: 30.0,color: Colors.black),),),
        Text(scheduleSub.term.startsAt + " - " + scheduleSub.term.endsAt,style: TextStyle(fontSize: 20.0,),textAlign: TextAlign.right,),
        Divider()
      ],
      ),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0),color: Colors.green),
      ),
      );
    }else{
    return FlatButton(child: Container(child: Column(children: <Widget>[
      Center(child: Text(scheduleSub.subject.name,style: TextStyle(fontSize: 30.0,color: Colors.black),),),
      Text(scheduleSub.term.startsAt + " - " + scheduleSub.term.endsAt,style: TextStyle(fontSize: 20.0,),textAlign: TextAlign.right,),
      Divider()
    ],)
    ),
    );
    } //Это костыль, не обращайте внимания,  вряд ли кто то будет копаться в этом коде кроме меня, и знаю я что так делать нельзя
  }
 
}