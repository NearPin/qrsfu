import 'dart:io';

import 'package:flutter/material.dart';
import 'BLoC.dart';
import 'AuthenticationForm.dart';

class Registration extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => RegistrationState();

}

class RegistrationState extends State{
  static final int STUDENT = 0;
  static final int TEACHER = 1;
  String _firstname;
  String _lastname;
  String _email;
  String _password;
  String _patronymic;
  String _group;
  int _userType = 0;
  BLoC bloc = new BLoC();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  
  final _formKey  = GlobalKey<FormState>();
  ListView widgets;
  ListView getListView(int _typeOfUser){
    if(_typeOfUser == STUDENT){
      return new ListView(children: <Widget>[

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Имя'
        ),
          validator: (value){
            if(value.isEmpty)
              return 'Пожайлуста, введите ваше имя';
            else{
              _firstname = value;
              return null;
            }
          },
        ),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Фамилия'
        ),
          validator: (value){
            if(value.isEmpty){
              return 'Пожайлуста, ведите вашу фамилию';
            }else{
              _lastname = value;
              return null;
            }
          },
        ),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Отчество'
        ),
          validator: (value){
            if(value.isEmpty){
              return 'Пожайлуста, введите ваше отчество';
            }{
              _patronymic = value;
              return null;
            }
          },),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'E-mail'
        ),
            validator: (value){
              if (value.isEmpty) return 'Пожалуйста, введите свой Email';

              String p = "[a-zA-Z0-9+.\_\%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})+";
              RegExp regExp = new RegExp(p);

              if (regExp.hasMatch(value)) {
                _email = value;
                return null;
              }

              return 'Это не E-mail';
            }),

        new SizedBox(height: 20.0,),

        new TextFormField(obscureText: true, decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Пароль'
        ),
          validator: (value){
            if(value.isEmpty) {
              return "Пожайлуста, придумайте пароль";
            }else{
              _password = value;
              return null;
            }
          },
        ),

        new SizedBox(height: 20.0,),

        new RadioListTile(title: const Text('Студент'),value: STUDENT, groupValue: _userType, onChanged: (int value){setState(() {
          _userType = value;
        });},),
        new RadioListTile(title: const Text('Преподаватель'),value: TEACHER, groupValue: _userType, onChanged: (int value){setState(() {
          _userType = value;
          _group = null;
        });},),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Группа'
        ),
            validator: (value){
          if(value.isEmpty){
            return 'Пожайлуста, введите номер группы (КИ19-11Б)';
          }else{
              _group = value;
              return null;
          }
            },
        ),

        new RaisedButton(onPressed: () async {

          if(_formKey.currentState.validate()) {
            String IMEI = await bloc.getIMEI();
            int statuseCode = await bloc.registrationUser(_firstname, _lastname, _patronymic, _email, _password, _userType, IMEI, _group);
            if(statuseCode==BLoC.SUCCESFULL_RESOPONCE_CODE){
              _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Регистрация успешно завершена'),backgroundColor: Colors.green,));
              Future.delayed(const Duration(seconds: 2)).then((_){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Authentication()));
              });
            }
            else if (statuseCode==BLoC.REGISTRATION_GROUP_NOT_EXIST){
              _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Указанной группы не существует'),backgroundColor: Colors.red,));
            }
            else{
              _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Регистрация не удалась'),backgroundColor: Colors.red,));
            }
          }

        },child: Text('Завершить регистрацию'), color: Colors.deepOrange, textColor: Colors.white,)

      ]);
    }else {
      return new ListView(children: <Widget>[

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Имя'
        ),
          validator: (value){
            if(value.isEmpty)
              return 'Пожайлуста, введите ваше имя';
            else{
              _firstname = value;
              return null;
            }
          },
        ),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Фамилия'
        ),
          validator: (value){
            if(value.isEmpty){
              return 'Пожайлуста, ведите вашу фамилию';
            }else{
              _lastname = value;
              return null;
            }
          },
        ),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Отчество'
        ),
          validator: (value){
            if(value.isEmpty){
              return 'Пожайлуста, введите ваше отчество';
            }{
              _patronymic = value;
              return null;
            }
          },),

        new SizedBox(height: 20.0,),

        new TextFormField(decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'E-mail'
        ),
            validator: (value){
              if (value.isEmpty) return 'Пожалуйста, введите свой Email';

              String p = "[a-zA-Z0-9+.\_\%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})+";
              RegExp regExp = new RegExp(p);

              if (regExp.hasMatch(value)) {
                _email = value;
                return null;
              }

              return 'Это не E-mail';
            }),

        new SizedBox(height: 20.0,),

        new TextFormField(obscureText: true, decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: 'Пароль'
        ),
          validator: (value){
            if(value.isEmpty) {
              return "Пожайлуста, придумайте пароль";
            }else{
              _password = value;
              return null;
            }
          },
        ),

        new SizedBox(height: 20.0,),

        new RadioListTile(title: const Text('Студент'),value: STUDENT, groupValue: _userType, onChanged: (int value){setState(() {
          _userType = value;
        });},),
        new RadioListTile(title: const Text('Преподаватель'),value: TEACHER, groupValue: _userType, onChanged: (int value){setState(() {
          _userType = value;
        });},),

        new SizedBox(height: 20.0,),

        new RaisedButton(onPressed: () async {

          if(_formKey.currentState.validate()) {
            String IMEI = await bloc.getIMEI();
            int statusCode = await bloc.registrationUser(_firstname, _lastname, _patronymic, _email, _password, _userType, IMEI,_group);
            if(statusCode == BLoC.SUCCESFULL_RESOPONCE_CODE){
              _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Регистрация успешно завершена'),backgroundColor: Colors.green,));
              Future.delayed(const Duration(seconds: 2)).then((_){
                Navigator.push(context, MaterialPageRoute(builder: (context)=>Authentication()));
              });
            }
            else if(statusCode==BLoC.REGISTRATION_GROUP_NOT_EXIST){
              _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Указанной группы не существует'),backgroundColor: Colors.red,));
            }
            else{
              _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Регистрация не удалась'),backgroundColor: Colors.red,));
            }
          }

        },child: Text('Завершить регистрацию'), color: Colors.deepOrange, textColor: Colors.white,)

      ]);
    }
  }

  @override
  Widget build(BuildContext context) {
    widgets = getListView(_userType);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(title: Text('SFU QR'),backgroundColor: Colors.deepOrange,),
      body: Builder( builder: (context) => Container(padding: EdgeInsets.all(10.0), child: new Form(key: _formKey, child: widgets)))
    );
  }

}