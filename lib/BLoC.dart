import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import 'User.dart';

class BLoC{
  User user;
  static final int SUCCESFULL_RESOPONCE_CODE = 0;
  static final int BAD_RESPONSE_CODE = 1;
  static final int AUTHENTICATE_TRYING_TO_ENTER_FROM_ALIEN_DEVICE_CODE = 2;
  static final int REGISTRATION_GROUP_NOT_EXIST = 2;
  Position newPosition;
  String serviceUrl = 'https://sibattendance.azurewebsites.net/';
  Future<String> getPosition() async {
    try {
      newPosition = await Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).timeout(new Duration(seconds: 5));
    } catch (e) {
      print('Error: ${e.toString()}');
    }
    return newPosition.longitude.toString() + "/" + newPosition.latitude.toString();
  }
  Future<String> getIMEI(){
    return ImeiPlugin.getImei();
  }

  Future<int> registrationUser(String firstName, String lastName, String patronymic, String email, String password, int userType, String IMEI, String group) async {
    var response = await http.post(serviceUrl + 'api/Account/register',body: jsonEncode(<String,dynamic>{
        "firstName": firstName,
        "lastName": lastName,
        "patronymic": patronymic,
        "email": email,
        "password": password,
        "userType": userType,
        "mobileID": IMEI,
        "group": group
      }),
      encoding: utf8,
      headers: {"Content-Type": "application/json",}
    ).catchError((error){
      print("$error");
    });
    try {
      if (response.statusCode == 200)
        return SUCCESFULL_RESOPONCE_CODE;
      if(response.statusCode==400){
        if(response.body== '{"message":"Указанной группы не существует."}'){
          return REGISTRATION_GROUP_NOT_EXIST;
        }
        return BAD_RESPONSE_CODE;
      }
    }catch(e){
      print(e);
      return BAD_RESPONSE_CODE;
    }
  }
  Future<List<ScheduleInfo>> getScheduleInfo () async {
    List<ScheduleInfo> schedules;
    var queryParams = {
      "id": user.id,
      "group": user.group,
      "weekType": getWeekType(),
      "dayOfWeek": getDayOfWeek(),
    };

    var uri = Uri.https("sibattendance.azurewebsites.net","api/Schedule",queryParams,);
    Response response = await http.get(uri,headers: {"Content-Type" : "application/json"},);
    List<ScheduleInfo> scheduleFromJson(String str) =>
          List<ScheduleInfo>.from(
              json.decode(str).map((x) => ScheduleInfo.fromJson(x)));
    schedules = scheduleFromJson(response.body);
    if(response.statusCode == 200){

      return schedules;
    }
    return new List<ScheduleInfo>();
  }
  List<ScheduleInfo> filterSchedules(List<ScheduleInfo> schedules){
    for(ScheduleInfo outItem in schedules){
        for(ScheduleInfo item in schedules){
          if(!identical(outItem, item) && identical(outItem.subject.name, item.subject.name) && identical(outItem.term.startsAt,item.term.startsAt)){

          }
        }
    }
  }
  Future<bool> postAttandancce(String itemID) async {
    String userToken = user.token;
    var response = await http.post(serviceUrl + "api/Attendance",body: jsonEncode(<String,dynamic>{
      "scheduleID": itemID,
      "userID": user.id
    }),
      headers: {"Content-Type": "application/json","Authorization" : "bearer $userToken" },
      encoding: utf8
    ).catchError((error){
      print("$error");
      });
    print(response.body);
    if(response.statusCode==200){
      return true;
    }else{
      return false;
    }
  }
  void getAttandance(String itemID){
    var queryParams = {
      "scheduleItemID": itemID,
      "userID": user.id,
      "attendanceDate": "string",
      "group": user.group
    };
  }
  Future<int> authenticateUser(String email,String password) async {
    String IMEI = await getIMEI();
    var response = await http.post(serviceUrl+'/api/Account/authenticate',body: jsonEncode(<String,String>{
      "username" : email,
      "password" : password,
      "mobileID" : IMEI
    }),

    encoding: utf8,
    headers: {"Content-Type" : "application/json"});
    AuthenticateUserJsonParser authenticateUserJsonParser;
    if(response.statusCode == 400) {
      print(response.body);
      if(response.body=="Попытка зайти с чужого устройства."){
        return AUTHENTICATE_TRYING_TO_ENTER_FROM_ALIEN_DEVICE_CODE;
      }
      return BAD_RESPONSE_CODE;
    }
    else if(response.statusCode == 200){
      print(response.body);
      authenticateUserJsonParser = AuthenticateUserJsonParser.fromJson(json.decode(response.body));
      setUserInfo(authenticateUserJsonParser);
      return SUCCESFULL_RESOPONCE_CODE;
    }

  }
  String getDayOfWeek(){
    DateTime dateTime = new DateTime.now();
    return dateTime.weekday.toString();
  }
  String getWeekType(){
    DateTime dateTime = new DateTime.now();
    int year = dateTime.year;
    DateTime startTime;
    if(dateTime.month<9){
      startTime = new DateTime(year - 1,9,1);
    } else{
      startTime = new DateTime(year,9,1);
    }
    int diff = dateTime.difference(startTime).inDays;
    if((diff~/7 + 1).isEven)
      return "even";
    return "odd";

  }
  void setUserInfo(AuthenticateUserJsonParser authenticateUserJsonParser){
    user = new User(authenticateUserJsonParser.userID, authenticateUserJsonParser.firstName, authenticateUserJsonParser.lastName,
        authenticateUserJsonParser.token,authenticateUserJsonParser.group);
  }
}
class ScheduleInfo{
  bool isAttendanced = false;
  final String id;
  final String group;
  final String weekType;
  final String dayOfWeek;
  final Term term;
  final String instructorName;
  final String classType;
  final Audience audience;
  final Subject subject;

  ScheduleInfo({
    this.id,
    this.group,
    this.weekType,
    this.dayOfWeek,
    this.term,
    this.instructorName,
    this.classType,
    this.audience,
    this.subject,
  });

  void attandanced(){
    isAttendanced = true;
  }

  factory ScheduleInfo.fromJson(Map<String, dynamic> json){
    if(json["audience"]==null){
      return ScheduleInfo(
          id: json["id"],
          group: json["group"],
          weekType: json["weekType"],
          dayOfWeek: json["dayOfWeek"],
          term: Term.fromJson(json["term"]),
          instructorName: json["instructorName"],
          classType: json["classType"],
          audience: Audience(),
          subject: Subject.fromJson(json["subject"]));
    }
    return ScheduleInfo(
    id: json["id"],
    group: json["group"],
    weekType: json["weekType"],
    dayOfWeek: json["dayOfWeek"],
    term: Term.fromJson(json["term"]),
    instructorName: json["instructorName"],
    classType: json["classType"],
    audience: Audience.fromJson(json["audience"]),
    subject: Subject.fromJson(json["subject"]),
  );
  }
}
class Audience {
  final int id;
  final String number;
  final String corpus;
  final dynamic border;

  Audience({
    this.id,
    this.number,
    this.corpus,
    this.border,
  });

  factory Audience.fromJson(Map<String, dynamic> json){
    return Audience(
    id: json["id"],
    number: json["number"],
    corpus: json["corpus"],
    border: json["border"],
  );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "number": number,
    "corpus": corpus,
    "border": border,
  };
}
class Subject {
  final int id;
  final String name;
  final dynamic description;

  Subject({
    this.id,
    this.name,
    this.description,
  });

  factory Subject.fromJson(Map<String, dynamic> json) => Subject(
    id: json["id"],
    name: json["name"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "description": description,
  };
}
class Term {
  final int classNumber;
  final String startsAt;
  final String endsAt;

  Term({
    this.classNumber,
    this.startsAt,
    this.endsAt,
  });

  factory Term.fromJson(Map<String, dynamic> json) => Term(
    classNumber: json["classNumber"],
    startsAt: json["startsAt"],
    endsAt: json["endsAt"],
  );

  Map<String, dynamic> toJson() => {
    "classNumber": classNumber,
    "startsAt": startsAt,
    "endsAt": endsAt,
  };
}
class AuthenticateUserJsonParser{
  final String userID;
  final String firstName;
  final String lastName;
  final String token;
  final String group;

  AuthenticateUserJsonParser({this.userID,this.firstName,this.lastName,this.token,this.group});
  factory AuthenticateUserJsonParser.fromJson(Map<String,dynamic> json){
    return AuthenticateUserJsonParser(
      userID: json['id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      token: json['token'],
      group: json['group']
    );
  }
}
