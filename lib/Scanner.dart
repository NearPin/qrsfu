import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qr_sfu/BLoC.dart';
import 'package:qr_sfu/Menu.dart';

class Scanner extends StatefulWidget{
  ScheduleInfo scheduleSub;
  BLoC bloc;
  Scanner(this.scheduleSub,this.bloc);
  @override
  State<StatefulWidget> createState() => ScannerState(scheduleSub,bloc);
}
class ScannerState extends State{
  Text text;
  BLoC bloc;
  ScheduleInfo scheduleSub;
  ScannerState(this.scheduleSub,this.bloc);
  String _barCodeResult = 'code is empty';
  @override
  Widget build(BuildContext context) {
    if(_barCodeResult=='code is empty')
    scanBarcodeNormal();
    if(_barCodeResult==scheduleSub.id){
        text = new Text("Вам удалось отметиться",style: TextStyle(fontSize: 20,color: Colors.green));
        bloc.postAttandancce(scheduleSub.id);
        scheduleSub.attandanced();

    }else
      {
       text = new Text("Вам не удалось отметиться", style: TextStyle(fontSize: 20,color: Colors.red));
      }
    return new Scaffold(
      appBar: AppBar(title:Text('SFU QR'),backgroundColor: Colors.deepOrange,),
      body: Container(child: Center( child: Column(children: <Widget>[
        new QrImage(
          data: scheduleSub.id,
          version: QrVersions.auto,
          size: 500.0,
        ),
        new Text("Покажите QR код вашему соседу",style: TextStyle(fontSize: 30),),
        text
      ]
        ,)
      ),
        padding: EdgeInsets.all(30),
      ),
    );
  }
  Future<bool> checkAttandace() async{
    return bloc.postAttandancce(scheduleSub.id);
}
  Future scanBarcodeNormal() async {
    String barcodeScanRes;
    barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
        "#ff6666", "Cancel", true, ScanMode.QR);
    setState(() {
      _barCodeResult = barcodeScanRes;
    });
  }
}